import { createSwitchNavigator } from "react-navigation";
import { TabsStackMain } from "./TabNavigation";
import { AuthStack, InitialStack } from "./StackNavigation";

const RootSwitchNavigator = createSwitchNavigator(
  {
    Initial: InitialStack,
    Home: TabsStackMain
  },
  {
    defaultNavigationOptions: {
      header: null
    },
    initialRoute: "Initial"
  }
);

export default RootSwitchNavigator;
