import React, { PureComponent } from "react";
import { Dimensions, View, Text, StyleSheet, Image } from "react-native";
import { ASSETS } from "../assets";
import { Colors } from "../assets/theme";
import { screenWidthPercentage } from "../helpers/utils";
export default class NavigationTabItem extends PureComponent {
  constructor(props) {
    super(props);
  }

  stylesTab = StyleSheet.create({
    viewTabLabel: {
      width: Dimensions.get("window").width,
      alignItems: "center",
      justifyContent: "center"
    },
    textLabel: {
      fontSize: 9
    },
    mainButtonContainer: {
      flexDirection: "column",
      justifyContent: "center",
      alignSelf: "center",
      paddingBottom: 10,
      position: "absolute"
    },
    mainButton: {
      borderRadius: 27.5,
      height: 55,
      width: 55,
      backgroundColor: Colors.blue
    }
  });

  mainButton = () => {
    return (
      <Image source={ASSETS.icons.qrcode} style={this.stylesTab.mainButton} />
    );
  };

  normalButton = () => {
    const { focused, tabIcon, baseColor, selectedColor } = this.props;
    return (
      <Image
        source={tabIcon}
        style={{
          resizeMode: "contain",
          tintColor: focused ? selectedColor : baseColor,
          height: 25,
          width: 25,
          marginBottom: 5
        }}
      />
    );
  };

  render() {
    const { focused, tabLabel, tabIcon, baseColor, selectedColor } = this.props;
    if (!tabIcon)
      return (
        <View
          style={{
            justifyContent: "flex-end",
            alignItems: "center",
            width: screenWidthPercentage(33)
          }}
        >
          <Text
            style={{
              color: focused ? selectedColor : baseColor,
              fontWeight: "600"
            }}
          >
            {tabLabel}
          </Text>
        </View>
      );
    return (
      <View style={{ flex: 1, justifyContent: "flex-end" }}>
        <View style={{ borderTopWidth: 1, alignSelf: "flex-start" }} />
        <View style={this.stylesTab.mainButtonContainer}>
          {this.normalButton()}
        </View>
        <View style={{ alignItems: "center", justifyContent: "center" }}>
          <Text
            style={{
              color: focused ? selectedColor : baseColor,
              fontSize: 11,
              fontWeight: "600"
            }}
          >
            {tabLabel}
          </Text>
        </View>
      </View>
    );
  }
}
