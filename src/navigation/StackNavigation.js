import { createStackNavigator } from "react-navigation";
import InitialContainer from "../containers/InitialContainer";
import RegisterContainer from "../containers/RegisterContainer";

// export const AuthStack = createStackNavigator({
//   Login: {
//     screen: LoginContainer,
//     navigationOptions: ({ navigation }) => ({
//       header: null
//     })
//   },
//   signIn: {
//     screen: ProfileRegistrationContainer,
//     navigationOptions: ({ navigation }) => ({
//       header: null
//     })
//   },
//   ForgotPass: {
//     screen: ForgotPassContainer,
//     navigationOptions: ({ navigation }) => ({
//       header: null
//     })
//   },
//   initialRouterName: "Login",
//   headerMode: "screen"
// });

export const InitialStack = createStackNavigator({
  Initial: {
    screen: InitialContainer,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  Register: {
    screen: RegisterContainer,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  initialRouterName: "Initial",
  headerMode: "screen"
});

// export const MoreStack = createStackNavigator({
//   MoreContainer: {
//     screen: MoreContainer,
//     navigationOptions: ({ navigation }) => ({
//       header: null
//     })
//   },
//   SettingsContainer: {
//     screen: SettingsContainer,
//     navigationOptions: ({ navigation }) => ({
//       header: null
//     })
//   },
//   ChangePassword4Container: {
//     screen: ChangePassword4Container,
//     navigationOptions: ({ navigation }) => ({
//       header: null
//     })
//   },
//   PersonalDataContainer: {
//     screen: PersonalDataContainer,
//     navigationOptions: ({ navigation }) => ({
//       header: null
//     })
//   },
//   ChangePasswordContainer: {
//     screen: ChangePasswordContainer,
//     navigationOptions: ({ navigation }) => ({
//       header: null
//     })
//   },
//   initialRouterName: "MoreContainer",
//   headerMode: "screen"
// });

// MoreStack.navigationOptions = ({ navigation }) => {
//   let tabBarVisible = true;
//   if (navigation.state.index > 0) {
//     tabBarVisible = false;
//   }
//   return {
//     tabBarVisible
//   };
// };
