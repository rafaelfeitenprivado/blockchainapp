import React, { PureComponent } from "react";
import { StyleSheet, View, Text, TouchableOpacity, Image } from "react-native";
import { responsiveSize } from "../helpers/utils";
import LinearGradient from "react-native-linear-gradient";

import { Colors } from "../assets/theme";
import { screenHeightPercentage } from "../helpers/utils";
import { ASSETS } from "../assets";

export default class NavigationHeader extends PureComponent {
  createLeftButtons = () => {
    const { showBackButton } = this.props;

    if (showBackButton) {
      const { pressBackButton } = this.props;
      return (
        <TouchableOpacity onPress={() => pressBackButton()}>
          <Image
            source={ASSETS.icons.arrow}
            style={[styles.imageButton, { marginLeft: 10 }]}
          />
        </TouchableOpacity>
      );
    } else {
      return null;
    }
  };

  render() {
    const { title, home, children } = this.props;

    return (
      // <LinearGradient
      //   colors={[Colors.redSecondary, Colors.red]}
      //   style={styles.content}
      //   start={{ x: 0, y: 0 }}
      //   end={{ x: 0, y: 1 }}
      // >

      <LinearGradient
        colors={[Colors.redSecondary, Colors.red]}
        style={styles.content}
        start={{ x: 0, y: 0 }}
        end={{ x: 0, y: 1 }}
      >
        {home ? (
          children
        ) : (
          <>
            <View style={styles.viewLeftButtons}>
              {this.createLeftButtons()}
            </View>
            <Text style={styles.title}>{title}</Text>
          </>
        )}
      </LinearGradient>
      // <View style={styles.content}>
      //   {home ? (
      //     children
      //   ) : (
      //     <>
      //       <View style={styles.viewLeftButtons}>
      //         {this.createLeftButtons()}
      //       </View>
      //       <Text style={styles.title}>{title}</Text>
      //     </>
      //   )}
      // </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: Colors.blue,
    height: screenHeightPercentage(13)
  },
  imageButton: {
    width: 30,
    height: 30,
    tintColor: Colors.white
  },
  title: {
    fontSize: responsiveSize(3.5),
    color: "white",
    fontWeight: "bold"
  },
  viewLeftButtons: {
    position: "absolute",
    flexDirection: "row",
    left: 0
  }
});

const stylesHeader = StyleSheet.create({
  content: {
    flexDirection: "row",
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    borderColor: Colors.gray,
    backgroundColor: Colors.red
  },
  container: {
    flex: 1
  },
  navBar: {
    justifyContent: "space-between",
    marginTop: screenHeightPercentage(5)
  },
  titleStyle: {
    color: Colors.white,
    fontWeight: "bold",
    fontSize: 18
  },
  imageButton: {
    width: 30,
    height: 30,
    tintColor: Colors.white
  }
});
