import React from "react";
import { createBottomTabNavigator } from "react-navigation";
import HomeContainer from "../containers/HomeContainer";
import TransferContainer from "../containers/TransferContainer";
import RecieveContainer from "../containers/RecieveContainer";
import { ASSETS } from "../assets";
import { Colors } from "../assets/theme";
import NavigationTabItem from "./NavigationTabItem";

export const TabsStackMain = createBottomTabNavigator(
  {
    Home: {
      screen: HomeContainer,
      navigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused }) => (
          <NavigationTabItem
            selectedColor={Colors.blue}
            baseColor={Colors.darkGray}
            tabIcon={ASSETS.icons.historico}
            focused={focused}
            tabLabel={"Histórico"}
          />
        )
      })
    },
    Transfer: {
      screen: TransferContainer,
      navigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused }) => (
          <NavigationTabItem
            selectedColor={Colors.blue}
            baseColor={Colors.darkGray}
            tabIcon={ASSETS.icons.transation}
            focused={focused}
            tabLabel={"Transferir"}
          />
        )
      })
    },
    Recieve: {
      screen: RecieveContainer,
      navigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused }) => (
          <NavigationTabItem
            selectedColor={Colors.blue}
            baseColor={Colors.darkGray}
            tabIcon={ASSETS.icons.qrcode}
            focused={focused}
            tabLabel={"Receber"}
            qrcode={true}
          />
        )
      })
    }
  },
  {
    animationEnabled: false,
    swipeEnabled: false,
    tabBarOptions: {
      showIcon: true,
      showLabel: false,
      style: {
        borderTopWidth: 1,
        shadowColor: Colors.black,
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.6,
        shadowRadius: 5,
        elevation: 5,
        backgroundColor: Colors.offWhite,
        borderColor: Colors.darkGray
      }
    }
  }
);
