import axios from "axios";
import AsyncStorage from "@react-native-community/async-storage";

const api = axios.create({
  baseURL: "http://localhost:3000",
  timeout: 10000,
  headers: {
    "Content-Type": "application/json"
  }
});

export const getAddress = () => {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem("address").then(address => {
      resolve(address);
    });
  });
};

export const saveAddress = address => {
  AsyncStorage.setItem("address", address);
};

export const createKeyPars = (userID, data) => {
  let url = `cadastro`;
  return new Promise((resolve, reject) => {
    api
      .get(url)
      .then(resp => {
        resolve(resp.data);
      })
      .catch(e => {
        console.log("e", e);
        reject(false);
      });
  });
};

export const getSaldo = address => {
  let url = `consultar/saldo/${address}`;
  return new Promise((resolve, reject) => {
    api
      .get(url)
      .then(resp => {
        resolve(resp.data.saldo);
      })
      .catch(e => {
        reject(false);
      });
  });
};

export const getHistorico = address => {
  let url = `consultar/historico/${address}`;
  return new Promise((resolve, reject) => {
    api
      .get(url)
      .then(resp => {
        resolve(resp.data.blocos);
      })
      .catch(e => {
        reject(false);
      });
  });
};

export const transfer = (
  addressEntrada,
  valorEntrada,
  addressSaida,
  valorSaida,
  taxa,
  wif,
  descricao
) => {
  let body = {
    inputs: [
      {
        address: addressEntrada,
        value: parseFloat(valorEntrada)
      }
    ],
    outputs: [
      {
        address: addressSaida,
        value: parseFloat(valorSaida)
      }
    ],
    fee_value: parseFloat(taxa),
    wif: wif,
    data: descricao
  };

  let url = `transferir`;
  return new Promise((resolve, reject) => {
    api
      .post(url, body)
      .then(resp => {
        resolve(resp.data.txid);
      })
      .catch(e => {
        reject(false);
      });
  });
};

export const recieve = (address, valor) => {
  let body = {
    address: address,
    value: parseFloat(valor)
  };

  let url = `receber`;
  return new Promise((resolve, reject) => {
    api
      .post(url, body)
      .then(resp => {
        console.log("resp", resp);
        resolve(resp.data.txid);
      })
      .catch(e => {
        reject(false);
      });
  });
};
