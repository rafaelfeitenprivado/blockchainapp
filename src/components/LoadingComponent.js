import React, { Component } from "react";
import { View, ActivityIndicator } from "react-native";
import { Colors } from "../assets/theme";
export default class LoadingComponent extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { size, style, color } = this.props;

    return (
      <ActivityIndicator
        size={size}
        color={color || Colors.blue}
        style={style}
      />
    );
  }
}
