import React, { Component } from "react";
import HomeComponent from "./HomeComponent";

import {
  getAddress,
  getSaldo,
  getHistorico
} from "../../helpers/databaseHelpers";

export default class HomeController extends Component {
  constructor(props) {
    super(props);
    this.state = {
      address: null,
      isLoading: true,
      erro: false,
      saldo: null,
      content: [],
      isRefreshing: false
    };
  }

  componentDidMount() {
    this.getSaldo();
  }

  getSaldo = refresh => {
    this.setState({ isRefreshing: refresh || false }, () => {
      getAddress()
        .then(address => {
          getSaldo(address)
            .then(saldo => {
              getHistorico(address)
                .then(content => {
                  this.setState({
                    address,
                    isLoading: false,
                    isRefreshing: false,
                    saldo,
                    content
                  });
                })
                .catch(() => {
                  this.setState({
                    erro: true,
                    isLoading: false,
                    isRefreshing: false
                  });
                });
            })
            .catch(() => {
              this.setState({
                erro: true,
                isLoading: false,
                isRefreshing: false
              });
            });
        })
        .catch(() => {
          this.setState({ erro: true, isLoading: false, isRefreshing: false });
        });
    });
  };

  render() {
    return (
      <HomeComponent
        isLoading={this.state.isLoading}
        erro={this.state.erro}
        address={this.state.address}
        saldo={this.state.saldo}
        content={this.state.content}
        getSaldo={this.getSaldo}
        isRefreshing={this.state.isRefreshing}
      />
    );
  }
}
