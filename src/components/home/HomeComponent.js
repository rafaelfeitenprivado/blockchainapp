import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  RefreshControl
} from "react-native";
import { Colors } from "../../assets/theme";
import { ASSETS } from "../../assets";
import { responsiveSize } from "../../helpers/utils";
import LoadingComponent from "../LoadingComponent";
import { formatTime } from "../../helpers/utils";
export default class HomeComponent extends Component {
  renderHeader = () => {
    const { saldo } = this.props;
    return (
      <View style={styles.general}>
        <View style={{ marginVertical: responsiveSize(1) }}>
          <Text
            style={{
              color: Colors.greenLight,
              fontSize: responsiveSize(3)
            }}
          >
            {`Recebido: ${saldo.totalReceived} BTC`}
          </Text>
        </View>
        <View style={{ marginVertical: responsiveSize(1) }}>
          <Text
            style={{
              color: Colors.redOpacity,
              fontSize: responsiveSize(3)
            }}
          >
            {`Enviado: ${saldo.totalSpent} BTC`}
          </Text>
        </View>
        <View style={{ marginVertical: responsiveSize(1) }}>
          <Text
            style={{
              color: Colors.blue,
              fontSize: responsiveSize(3.3)
            }}
          >
            {`Saldo: ${saldo.balance} BTC`}
          </Text>
        </View>
        <View style={{ marginTop: responsiveSize(2) }}>
          <Text
            style={{
              color: Colors.black,
              fontSize: responsiveSize(3),
              textAlign: "center"
            }}
          >
            Histórico
          </Text>
        </View>
      </View>
    );
  };

  renderItem = ({ item }) => {
    return (
      <View style={cardStyle.itemContainer}>
        <View style={cardStyle.row}>
          <Text>{formatTime(item.timestamp)}</Text>
          <Text
            style={{
              color: item.confirmations < 6 ? Colors.red : Colors.green
            }}
          >{`Confirmações: ${item.confirmations}`}</Text>
        </View>
        <View style={cardStyle.margin} />
        <Text style={cardStyle.section}>Transação</Text>
        <Text style={cardStyle.hash}>{item.hash}</Text>
        <View style={cardStyle.margin} />
        <Text style={cardStyle.section}>
          {item.txins.length > 1
            ? `Transações de entrada`
            : `Transação de entrada`}
        </Text>
        {item.txins.map(transacao => {
          return (
            <View style={cardStyle.transacaoInnerContainer}>
              <Text>Hash:</Text>
              <Text style={cardStyle.hash}>{transacao.txout}</Text>
              <View style={cardStyle.margin} />
              <Text>{`Valor: ${transacao.amount} BTC`}</Text>
              <View style={cardStyle.margin} />
              <Text>{`Address: ${transacao.addresses}`}</Text>
            </View>
          );
        })}
        <View style={cardStyle.margin} />
        <Text style={cardStyle.section}>
          {item.txouts.length > 1
            ? `Transações de saída`
            : `Transação de saída`}
        </Text>
        {item.txouts.map(transacao => {
          if (transacao.addresses.length > 0)
            return (
              <View style={cardStyle.transacaoInnerContainer}>
                <View style={cardStyle.margin} />
                <Text>{`Valor: ${transacao.amount} BTC`}</Text>
                <View style={cardStyle.margin} />
                <Text>{`Address: ${transacao.addresses}`}</Text>
              </View>
            );
        })}
        <View style={cardStyle.margin} />
        <Text style={cardStyle.section}>Hash</Text>
        <Text style={cardStyle.hash}>{item.blockhash}</Text>
      </View>
    );
  };

  render() {
    const {
      isLoading,
      erro,
      address,
      content,
      getSaldo,
      isRefreshing
    } = this.props;
    if (isLoading) {
      return (
        <View
          style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
        >
          <LoadingComponent size={"large"} />
        </View>
      );
    }

    if (erro) {
      return (
        <View
          style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
        >
          <Image
            source={ASSETS.icons.sad}
            style={{
              tintColor: Colors.blue
            }}
          />
          <Text
            style={{
              color: Colors.blue,
              fontSize: responsiveSize(3),
              textAlign: "center"
            }}
          >
            Desculpe, tivemos um erro inesperado!
          </Text>
        </View>
      );
    }
    return (
      <View style={{ flex: 1 }}>
        <View style={{ marginTop: responsiveSize(4), alignItems: "center" }}>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginBottom: responsiveSize(2)
            }}
          >
            <Image
              source={ASSETS.icons.qrcode}
              style={{
                tintColor: Colors.blue,
                height: responsiveSize(5),
                width: responsiveSize(5)
              }}
            />
            <Text
              style={{
                color: Colors.blue,
                fontSize: responsiveSize(3)
              }}
            >
              Bitcoin Testnet Address
            </Text>
          </View>
          <View style={{ marginBottom: responsiveSize(2) }}>
            <Text
              style={{
                fontSize: responsiveSize(2)
              }}
            >
              {address}
            </Text>
          </View>
        </View>
        <FlatList
          ListHeaderComponent={this.renderHeader}
          data={content}
          keyExtractor={item => item.hash}
          refreshControl={
            <RefreshControl
              refreshing={isRefreshing}
              onRefresh={() => getSaldo(true)}
            />
          }
          showsVerticalScrollIndicator={false}
          renderItem={this.renderItem}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  general: {
    flex: 1,
    marginHorizontal: responsiveSize(1)
  },
  welcome: {
    textAlign: "center",
    color: "black"
  },
  buttonContainer: {
    borderWidth: 1,
    borderColor: "black",
    borderRadius: 10,
    paddingVertical: 10,
    width: responsiveSize(25),
    marginBottom: responsiveSize(2)
  }
});

const cardStyle = StyleSheet.create({
  itemContainer: {
    backgroundColor: Colors.white,
    flex: 1,
    margin: responsiveSize(1.5),
    paddingVertical: responsiveSize(1.5),
    paddingHorizontal: responsiveSize(2),
    borderRadius: responsiveSize(1.5),
    shadowColor: Colors.black,
    elevation: 3,
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.4,
    shadowRadius: 3
  },
  transacaoInnerContainer: {
    marginVertical: responsiveSize(1),
    borderColor: Colors.lightGray,
    borderWidth: responsiveSize(0.2),
    borderRadius: 10,
    padding: responsiveSize(1),
    shadowColor: Colors.darkGray,
    elevation: 3,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 2,
    backgroundColor: Colors.grayLight
  },
  hash: {
    textAlign: "center"
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  section: {
    textAlign: "center",
    color: Colors.blue,
    textDecorationLine: "underline",
    fontSize: responsiveSize(2.2)
  },
  margin: {
    marginVertical: responsiveSize(1)
  }
});
