import React, { Component } from "react";
import {
  View,
  ActivityIndicator,
  Animated,
  Image,
  Text,
  Keyboard,
  TouchableOpacity,
  Platform
} from "react-native";
import { StyleSheet, Dimensions } from "react-native";

import { Colors } from "../../assets/theme";
import { ASSETS } from "../../assets";
import ButtonComponent from "../ButtonComponent";
import TextInput from "../TextInput";
import LoadingComponent from "../LoadingComponent";

const window = Dimensions.get("window");
const IMAGE_HEIGHT = window.height / 4;
const IMAGE_HEIGHT_SMALL = window.height / 5;

class LoginComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showKeyboard: false
    };
    this.imageHeight = new Animated.Value(IMAGE_HEIGHT);
  }
  renderAndroid() {
    const { showKeyboard } = this.state;

    if (showKeyboard === true) {
      return (
        <Image
          source={ASSETS.images.logo}
          style={[styles.logo, { height: IMAGE_HEIGHT_SMALL }]}
        />
      );
    } else {
      return (
        <Image
          source={ASSETS.images.logo}
          style={[styles.logo, { height: IMAGE_HEIGHT }]}
        />
      );
    }
  }

  renderIOS() {
    return (
      <Animated.Image
        source={ASSETS.images.logo}
        style={[styles.logo, { height: this.imageHeight }]}
      />
    );
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this._keyboardDidShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this._keyboardDidHide
    );
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = event => {
    this.setState({ showKeyboard: true }, () => {
      Platform.OS === "ios"
        ? Animated.timing(this.imageHeight, {
            duration: event.duration,
            toValue: IMAGE_HEIGHT_SMALL
          }).start()
        : null;
    });
  };

  _keyboardDidHide = event => {
    this.setState({ showKeyboard: false }, () => {
      Platform.OS === "ios"
        ? Animated.timing(this.imageHeight, {
            duration: event.duration,
            toValue: IMAGE_HEIGHT
          }).start()
        : null;
    });
  };

  renderButton() {
    const { isLoading, tryLogin } = this.props;
    if (isLoading) {
      return <LoadingComponent />;
    }
    return (
      <ButtonComponent
        label={"Entrar"}
        onPress={tryLogin}
        clickable={this.clickableTrue()}
      />
    );
  }

  clickableTrue = () => {
    const { email, password } = this.props;
    if (email !== "" && password !== "") {
      return true;
    } else {
      return false;
    }
  };
  render() {
    const {
      email,
      password,
      onChangeHandler,
      isLoadingAuth,
      signIn,
      forgotPass
    } = this.props;
    if (isLoadingAuth) {
      return (
        <View style={styles.active}>
          <LoadingComponent size="large" />
        </View>
      );
    }
    return (
      <View
        style={[
          styles.container,
          { paddingTop: this.state.showKeyboard ? "5%" : "20%" }
        ]}
        behavior="padding"
      >
        {Platform.OS === "ios" && this.renderIOS()}
        {Platform.OS === "android" && this.renderAndroid()}

        <View style={{ width: "85%" }}>
          <TextInput
            label={"Email"}
            value={email}
            onSubmitEditing={Keyboard.dismiss}
            fontSize={18}
            keyboardType={"email-address"}
            onChangeText={rawText => onChangeHandler("email", rawText)}
          />
          <View style={{ marginVertical: "3%" }} />
          <TextInput
            label={"Senha"}
            value={password}
            onSubmitEditing={Keyboard.dismiss}
            fontSize={18}
            isSecure={true}
            onChangeText={rawText => onChangeHandler("password", rawText)}
          />
        </View>
        <View style={{ margin: 30 }}>{this.renderButton()}</View>
        <View style={styles.text}>
          <View style={{ flexDirection: "row" }}>
            <Text>Ainda não possui uma conta? </Text>
            <TouchableOpacity
              onPress={() => {
                signIn();
              }}
            >
              <Text style={[styles.text2, styles.text3]}>Registre-se</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            marginTop: "5%"
          }}
        >
          <TouchableOpacity
            onPress={() => {
              forgotPass();
            }}
          >
            <Text style={[styles.text2]}>Esqueceu sua senha?</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
export default LoginComponent;
const styles = StyleSheet.create({
  container: {
    alignItems: "center"
  },
  active: {
    flex: 1,
    alignSelf: "center",
    justifyContent: "center"
  },
  logo: {
    height: IMAGE_HEIGHT,
    resizeMode: "contain",
    marginBottom: 20,
    marginTop: 20,
    tintColor: Colors.blue
  },
  register: {
    marginBottom: 20,
    width: window.width - 100,
    alignItems: "center",
    justifyContent: "center",
    height: 50,
    backgroundColor: "#ffae"
  },
  text: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  text2: {
    color: Colors.blue,
    textDecorationLine: "underline",
    fontSize: 16,
    bottom: 2
  },
  text3: {
    fontWeight: "500"
  }
});
