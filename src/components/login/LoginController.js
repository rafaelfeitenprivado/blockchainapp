import React from "react";
import LoginComponent from "./LoginComponent";
import { messageByErrorCode } from "../../helpers/databaseUtils";
import {
  signIn,
  isUserAuth,
  saveHistoricoLogin
} from "../../helpers/databaseHelpers";
import { Alert } from "react-native";
import { getDeviceInfos, getTime } from "../../helpers/utils";

export default class LoginController extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      isLoading: false,
      isLoadingAuth: true,
      message: "",
      infos: null
    };
    this.veirfyUserLogged();
  }

  veirfyUserLogged = () => {
    isUserAuth()
      .then(user => {
        this.setState({ isLoadingAuth: false }, () =>
          this.props.navigation.navigate("Home")
        );
      })
      .catch(error => {
        this.getDeviceInfos();
        this.setState({ isLoadingAuth: false });
      });
  };

  getDeviceInfos = () => {
    let infos = getDeviceInfos();
    this.setState({ infos });
  };

  onChangeHandler = (item, value) => {
    this.setState({
      [item]: value
    });
  };

  tryLogin = () => {
    this.setState({ isLoading: true, message: "" });
    const { email, password, infos } = this.state;
    const { navigation } = this.props;
    signIn(email, password)
      .then(data => {
        let userID = data.user.uid;
        let time = getTime();
        let historico = { ...infos, time };
        saveHistoricoLogin(userID, historico);
        this.setState({ isLoading: false }, () => {
          navigation.navigate("Home");
        });
      })
      .catch(error => {
        this.setState(
          {
            message: messageByErrorCode(error.code),
            isLoading: false
          },
          () => {
            this.renderMessage();
          }
        );
      });
  };

  renderMessage() {
    const { message } = this.state;
    if (!message) {
      return null;
    } else {
      return Alert.alert("", message);
    }
  }

  signIn = () => {
    this.props.navigation.navigate("signIn");
  };

  forgotPass = () => {
    this.props.navigation.navigate("ForgotPass");
  };

  render() {
    return (
      <LoginComponent
        email={this.state.email}
        password={this.state.password}
        message={this.state.message}
        onChangeHandler={this.onChangeHandler}
        tryLogin={this.tryLogin}
        isLoading={this.state.isLoading}
        isLoadingAuth={this.state.isLoadingAuth}
        signIn={this.signIn}
        forgotPass={this.forgotPass}
      />
    );
  }
}
