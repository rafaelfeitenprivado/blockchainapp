import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  DeviceEventEmitter,
  Image,
  TouchableOpacity,
  FlatList
} from "react-native";
import { Colors } from "../../assets/theme";
import { ASSETS } from "../../assets";
import { responsiveSize } from "../../helpers/utils";
import LoadingComponent from "../LoadingComponent";
import { formatTime } from "../../helpers/utils";
import TextInput from "../TextInput";
import ButtonComponent from "../ButtonComponent";

export default class RecieveComponent extends Component {
  canLimpar = () => {
    const { valor } = this.props;
    if (valor !== "") {
      return true;
    } else {
      return false;
    }
  };

  canEnviar = () => {
    const { valor } = this.props;

    if (parseFloat(valor) <= 0.01) {
      return true;
    }
    return false;
  };

  render() {
    const {
      isLoading,
      erro,
      address,
      valor,
      onChangeHandler,
      limparState,
      isLoadingSend,
      txid,
      recieveValue
    } = this.props;

    if (isLoading) {
      return (
        <View
          style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
        >
          <LoadingComponent size={"large"} />
        </View>
      );
    }

    if (erro) {
      return (
        <View
          style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
        >
          <Image
            source={ASSETS.icons.sad}
            style={{
              tintColor: Colors.blue
            }}
          />
          <Text
            style={{
              color: Colors.blue,
              fontSize: responsiveSize(3),
              textAlign: "center"
            }}
          >
            Desculpe, tivemos um erro inesperado!
          </Text>
        </View>
      );
    }

    return (
      <View
        style={{
          flex: 1
        }}
      >
        <View style={{ marginTop: responsiveSize(5), alignItems: "center" }}>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginBottom: responsiveSize(2)
            }}
          >
            <Image
              source={ASSETS.icons.qrcode}
              style={{
                tintColor: Colors.blue,
                height: responsiveSize(5),
                width: responsiveSize(5)
              }}
            />
            <Text
              style={{
                color: Colors.blue,
                fontSize: responsiveSize(3)
              }}
            >
              Bitcoin Testnet Address
            </Text>
          </View>
        </View>
        <View style={{ justifyContent: "center", flex: 1 }}>
          <View style={styles.general}>
            <Text style={styles.welcome}>Receber Bitcoins</Text>
            <Text style={styles.welcome4}>
              Este é um serviço oferecido de forma gratuita, use de forma
              consciente. Quando não utilizar mais os bitcoins, favor
              devolvê-los para o endereço abaixo:
            </Text>
            <Text style={styles.welcome3}>
              n3JBqkUAxDE42t1HtLXJDWQMid4N2vnxi1
            </Text>
          </View>
          <View style={styles.general}>
            <TextInput
              label={"Address"}
              editable={false}
              value={address}
            ></TextInput>
          </View>
          <View style={styles.general}>
            <TextInput
              label={"Quantidade a receber"}
              type={"money"}
              options={{
                precision: 8,
                separator: ".",
                delimiter: ",",
                unit: "",
                suffixUnit: "BTC"
              }}
              value={valor}
              onChangeText={rawText => onChangeHandler("valor", rawText)}
            ></TextInput>
          </View>
          <Text style={styles.welcome4}>Limite de 0.01 BTC por transação.</Text>
          <Text style={styles.welcome3}>
            {isLoadingSend
              ? "Transferindo..."
              : txid
              ? `Transação realizada com sucesso!`
              : ""}
          </Text>
          <Text style={styles.welcome2}>{txid ? txid : ""}</Text>

          <View
            style={{
              flexDirection: "row",
              justifyContent: "flex-end",
              position: "absolute",
              bottom: responsiveSize(1)
            }}
          >
            <View
              style={{
                flex: 1,
                marginHorizontal: 10
              }}
            >
              <ButtonComponent
                label={
                  isLoadingSend ? (
                    <LoadingComponent color={"white"} />
                  ) : (
                    "Receber"
                  )
                }
                clickable={this.canEnviar()}
                onPress={() => recieveValue()}
              />
            </View>
            <View
              style={{
                flex: 1,
                marginHorizontal: 10
              }}
            >
              <ButtonComponent
                label={"Limpar"}
                clickable={this.canLimpar()}
                onPress={() => limparState()}
              />
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  general: {
    marginHorizontal: responsiveSize(1),
    marginVertical: responsiveSize(3)
  },
  welcome: {
    textAlign: "center",
    color: Colors.blue,
    fontSize: responsiveSize(3)
  },
  welcome2: {
    textAlign: "center",
    color: Colors.blue,
    marginTop: responsiveSize(1),
    marginHorizontal: responsiveSize(2),
    fontSize: responsiveSize(2)
  },
  welcome3: {
    textAlign: "center",
    color: Colors.blue,
    marginTop: responsiveSize(2),
    fontSize: responsiveSize(2),
    fontWeight: "bold"
  },
  welcome4: {
    textAlign: "center",
    marginTop: responsiveSize(2),
    fontSize: responsiveSize(1.8)
  },
  buttonContainer: {
    borderWidth: 1,
    borderColor: "black",
    borderRadius: 10,
    paddingVertical: 10,
    width: responsiveSize(25),
    marginBottom: responsiveSize(2)
  }
});
