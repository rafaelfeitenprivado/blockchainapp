import React, { Component } from "react";
import RecieveComponent from "./RecieveComponent";
import { getAddress, recieve } from "../../helpers/databaseHelpers";

export default class RecieveController extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      isLoadingSend: false,
      erro: false,
      address: null,
      valor: ""
    };
    this.getEndereco();
  }

  getEndereco = () => {
    getAddress()
      .then(address => {
        this.setState({ address, isLoading: false });
      })
      .catch(() => {
        this.setState({ erro: true, isLoading: false });
      });
  };

  onChangeHandler = (item, value) => {
    this.setState({
      [item]: value
    });
  };

  recieveValue = () => {
    const { address, valor } = this.state;
    this.setState({ isLoadingSend: true }, () => {
      recieve(address, valor)
        .then(resp => {
          this.setState({ txid: resp.txid, isLoadingSend: false });
        })
        .catch(e => {
          this.setState({ erro: true, isLoadingSend: false });
        });
    });
  };

  limparState = () => {
    this.setState({
      valor: "",
      txid: null,
      isLoadingSend: false
    });
  };

  render() {
    return (
      <RecieveComponent
        isLoading={this.state.isLoading}
        isLoadingSend={this.state.isLoadingSend}
        erro={this.state.erro}
        recieveValue={this.recieveValue}
        address={this.state.address}
        valor={this.state.valor}
        onChangeHandler={this.onChangeHandler}
        limparState={this.limparState}
        txid={this.state.txid}
      />
    );
  }
}
