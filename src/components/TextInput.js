import React, { Component } from "react";
import { View, Text, Platform, TextInput } from "react-native";
import { TextInputMask } from "react-native-masked-text";
import { Colors } from "../assets/theme";
import {
  screenHeightPercentage,
  screenWidthPercentage
} from "../helpers/utils";
export default class TextField extends Component {
  state = {
    isFocused: false
  };

  handleFocus = () => this.setState({ isFocused: true });
  handleBlur = () => this.setState({ isFocused: false });

  checkLabel = (isFocused, value) => {
    if (isFocused) return true;
    else {
      if (value === "") return false;
      else return true;
    }
  };

  renderInput() {
    const {
      type,
      typePassword,
      isSecure,
      autoCorrect,
      label,
      titleColor,
      lineColor,
      valueColor,
      ...props
    } = this.props;
    const { isFocused } = this.state;
    if (type) {
      return (
        <TextInputMask
          type={type}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          secureTextEntry={isSecure}
          includeRawValueInChangeText={true}
          style={{ color: Colors.black }}
          underlineColorAndroid={"black"}
          {...props}
        />
      );
    } else if (typePassword) {
      return (
        <TextInput
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          autoCapitalize={"none"}
          style={{
            color: valueColor ? valueColor : Colors.black
          }}
          autoCorrect={autoCorrect ? autoCorrect : false}
          secureTextEntry={isSecure}
          underlineColorAndroid={lineColor ? lineColor : "black"}
          {...props}
        />
      );
    } else {
      return (
        <TextInput
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          autoCapitalize={"none"}
          style={{
            color: valueColor ? valueColor : Colors.black
          }}
          autoCorrect={autoCorrect ? autoCorrect : false}
          secureTextEntry={isSecure}
          underlineColorAndroid={lineColor ? lineColor : "black"}
          {...props}
        />
      );
    }
  }

  render() {
    const { label, value, fontSize, titleColor } = this.props;
    const { isFocused } = this.state;

    return (
      <View
        style={{
          paddingTop: Platform.OS === "ios" ? 18 : 0,
          borderBottomColor: "black",
          borderBottomWidth: Platform.OS === "ios" ? 1 : 0
        }}
      >
        <Text
          style={{
            position: "absolute",
            fontSize: fontSize
              ? !this.checkLabel(isFocused, value)
                ? fontSize
                : fontSize - 4
              : !this.checkLabel(isFocused, value)
              ? 16
              : 12,
            color: titleColor ? titleColor : "black",
            marginBottom:
              Platform.OS === "ios" ? 0 : screenHeightPercentage(-2.5),
            top: this.checkLabel(isFocused, value)
              ? screenHeightPercentage(-0.5)
              : screenHeightPercentage(2.5),
            marginLeft: Platform.OS === "ios" ? 0 : 5
          }}
        >
          {label}
        </Text>
        {this.renderInput()}
      </View>
    );
  }
}
