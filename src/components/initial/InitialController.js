import React from "react";
import InitialComponent from "./InitialComponent";
import { saveAddress } from "../../helpers/databaseHelpers";
export default class InitialController extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      address: ""
    };
  }

  onChangeHandler = (item, value) => {
    this.setState({
      [item]: value
    });
  };

  registrar = () => {
    this.props.navigation.navigate("Register");
  };

  entrar = () => {
    saveAddress(this.state.address);
    this.props.navigation.navigate("Home");
  };

  render() {
    return (
      <InitialComponent
        address={this.state.address}
        entrar={this.entrar}
        registrar={this.registrar}
        onChangeHandler={this.onChangeHandler}
      />
    );
  }
}
