import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  DeviceEventEmitter,
  Image,
  TouchableOpacity,
  FlatList
} from "react-native";
import { Colors } from "../../assets/theme";
import { ASSETS } from "../../assets";
import { responsiveSize } from "../../helpers/utils";
import LoadingComponent from "../LoadingComponent";
import { formatTime } from "../../helpers/utils";
import TextInput from "../TextInput";
import ButtonComponent from "../ButtonComponent";

export default class TransferComponent extends Component {
  canLimpar = () => {
    const {
      valorEntrada,
      valorSaida,
      addressSaida,
      fee,
      descricao,
      wif
    } = this.props;

    if (
      valorEntrada !== "" ||
      valorSaida !== "" ||
      fee !== "" ||
      descricao !== "" ||
      wif !== "" ||
      addressSaida !== ""
    ) {
      return true;
    } else {
      return false;
    }
  };

  canEnviar = () => {
    const { valorEntrada, valorSaida, addressSaida, fee, wif } = this.props;

    if (
      valorEntrada !== "" &&
      valorSaida !== "" &&
      fee !== "" &&
      wif !== "" &&
      addressSaida !== "" &&
      valorEntrada === valorSaida
    ) {
      return true;
    }
    return false;
  };

  render() {
    const {
      isLoading,
      erro,
      sendValue,
      address,
      valorEntrada,
      onChangeHandler,
      valorSaida,
      addressSaida,
      fee,
      descricao,
      wif,
      limparState,
      isLoadingSend,
      txid
    } = this.props;

    if (isLoading) {
      return (
        <View
          style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
        >
          <LoadingComponent size={"large"} />
        </View>
      );
    }

    return (
      <View
        style={{
          flex: 1,
          marginTop: responsiveSize(5)
        }}
      >
        <View>
          <Text style={styles.welcome}>Entrada</Text>
        </View>
        <View style={styles.general}>
          <TextInput
            label={"Address"}
            editable={false}
            value={address}
          ></TextInput>
        </View>
        <View style={styles.general}>
          <TextInput
            label={"Quantidade"}
            type={"money"}
            options={{
              precision: 8,
              separator: ".",
              delimiter: ",",
              unit: "",
              suffixUnit: "BTC"
            }}
            value={valorEntrada}
            onChangeText={rawText => onChangeHandler("valorEntrada", rawText)}
          ></TextInput>
        </View>
        <View style={{ marginTop: responsiveSize(2) }}></View>
        <View>
          <Text style={styles.welcome}>Saída</Text>
        </View>
        <View style={styles.general}>
          <TextInput
            label={"Address"}
            value={addressSaida}
            onChangeText={rawText => onChangeHandler("addressSaida", rawText)}
          ></TextInput>
        </View>
        <View style={styles.general}>
          <TextInput
            label={"Quantidade"}
            type={"money"}
            options={{
              precision: 8,
              separator: ".",
              delimiter: ",",
              unit: "",
              suffixUnit: "BTC"
            }}
            value={valorSaida}
            onChangeText={rawText => onChangeHandler("valorSaida", rawText)}
          ></TextInput>
        </View>

        <View style={styles.general}>
          <TextInput
            label={"Valor Taxa"}
            type={"money"}
            options={{
              precision: 8,
              separator: ".",
              delimiter: ",",
              unit: "",
              suffixUnit: "BTC"
            }}
            value={fee}
            onChangeText={rawText => onChangeHandler("fee", rawText)}
          ></TextInput>
        </View>

        <View style={styles.general}>
          <TextInput
            label={"WIF"}
            value={wif}
            onChangeText={rawText => onChangeHandler("wif", rawText)}
          ></TextInput>
        </View>

        <View style={styles.general}>
          <TextInput
            label={"Descrição"}
            value={descricao}
            onChangeText={rawText => onChangeHandler("descricao", rawText)}
          ></TextInput>
        </View>

        <Text style={styles.welcome3}>
          {isLoadingSend
            ? "Transferindo..."
            : txid
            ? `Transferencia realizada com sucesso!`
            : erro
            ? "Erro ao realizar a transferencia."
            : ""}
        </Text>
        <Text style={styles.welcome2}>{txid ? txid : ""}</Text>

        <View
          style={{
            flexDirection: "row",
            justifyContent: "flex-end",
            position: "absolute",
            bottom: responsiveSize(1)
          }}
        >
          <View
            style={{
              flex: 1,
              marginHorizontal: 10
            }}
          >
            <ButtonComponent
              label={
                isLoadingSend ? <LoadingComponent color={"white"} /> : "Enviar"
              }
              clickable={this.canEnviar()}
              onPress={() => sendValue()}
            />
          </View>
          <View
            style={{
              flex: 1,
              marginHorizontal: 10
            }}
          >
            <ButtonComponent
              label={"Limpar"}
              clickable={this.canLimpar()}
              onPress={() => limparState()}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  general: {
    marginHorizontal: responsiveSize(1),
    marginVertical: responsiveSize(2)
  },
  welcome: {
    textAlign: "center",
    color: Colors.blue,
    fontSize: responsiveSize(3)
  },
  welcome2: {
    textAlign: "center",
    color: Colors.blue,
    marginTop: responsiveSize(1),
    marginHorizontal: responsiveSize(2),
    fontSize: responsiveSize(2)
  },
  welcome3: {
    textAlign: "center",
    color: Colors.blue,
    marginTop: responsiveSize(2),
    fontSize: responsiveSize(2),
    fontWeight: "bold"
  },
  buttonContainer: {
    borderWidth: 1,
    borderColor: "black",
    borderRadius: 10,
    paddingVertical: 10,
    width: responsiveSize(25),
    marginBottom: responsiveSize(2)
  }
});
