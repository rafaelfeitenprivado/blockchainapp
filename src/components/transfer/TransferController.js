import React, { Component } from "react";
import TransferComponent from "./TransferComponent";
import { getAddress, transfer } from "../../helpers/databaseHelpers";

export default class TransferController extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      isLoadingSend: false,
      erro: false,
      address: null, //TODO=> get
      valorEntrada: "",
      wif: "",
      descricao: "",
      fee: "",
      valorSaida: "",
      addressSaida: "",
      txid: null
    };
    this.getEndereco();
  }

  getEndereco = () => {
    getAddress()
      .then(address => {
        this.setState({ address, isLoading: false });
      })
      .catch(() => {
        this.setState({ erro: true, isLoading: false });
      });
  };

  onChangeHandler = (item, value) => {
    this.setState({
      [item]: value
    });
  };

  sendValue = () => {
    const {
      address,
      valorEntrada,
      valorSaida,
      wif,
      descricao,
      fee,
      addressSaida
    } = this.state;

    this.setState({ isLoadingSend: true }, () => {
      transfer(
        address,
        valorEntrada,
        addressSaida,
        valorSaida,
        fee,
        wif,
        descricao
      )
        .then(txid => {
          this.setState({ txid, isLoadingSend: false });
        })
        .catch(e => {
          this.setState({ erro: true, isLoadingSend: false });
        });
    });
  };

  limparState = () => {
    this.setState({
      valorEntrada: "",
      wif: "",
      descricao: "",
      fee: "",
      valorSaida: "",
      addressSaida: "",
      txid: null,
      isLoadingSend: false
    });
  };

  render() {
    return (
      <TransferComponent
        isLoading={this.state.isLoading}
        isLoadingSend={this.state.isLoadingSend}
        erro={this.state.erro}
        sendValue={this.sendValue}
        address={this.state.address}
        valorEntrada={this.state.valorEntrada}
        saida={this.state.saida}
        wif={this.state.wif}
        descricao={this.state.descricao}
        onChangeHandler={this.onChangeHandler}
        valorSaida={this.state.valorSaida}
        addressSaida={this.state.addressSaida}
        fee={this.state.fee}
        limparState={this.limparState}
        txid={this.state.txid}
      />
    );
  }
}
