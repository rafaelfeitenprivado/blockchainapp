import React, { Component } from "react";
import { View, Text, TouchableOpacity, StyleSheet, Image } from "react-native";
import { Colors } from "../assets/theme";
import { ASSETS } from "../assets";
import { responsiveSize } from "../helpers/utils";

export default class ButtonComponent extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { label, clickable, onPress, type, width } = this.props;
    if (type == "first" || !type) {
      return (
        <View style={([stylesFirst.container], { width })}>
          <TouchableOpacity
            onPress={onPress}
            style={[
              stylesFirst.touchableContainer,
              clickable === true
                ? stylesFirst.clickable
                : stylesFirst.noClickable
            ]}
            disabled={clickable === true ? false : true}
          >
            <Text
              style={[
                stylesFirst.label,
                clickable === true ? stylesFirst.labelOn : stylesFirst.labelOff
              ]}
            >
              {label}
            </Text>
          </TouchableOpacity>
        </View>
      );
    } else {
      return (
        <View style={stylesSecond.container}>
          <TouchableOpacity
            onPress={onPress}
            style={stylesSecond.touchableContainer}
            disabled={clickable === true ? false : true}
          >
            <Image
              style={[
                stylesSecond.icon,
                { tintColor: clickable ? Colors.red : Colors.gray }
              ]}
              height={25}
              width={25}
              source={ASSETS.icons.add}
            />
            <Text style={stylesSecond.label}>{label}</Text>
          </TouchableOpacity>
        </View>
      );
      {
      }
    }
  }
}

const stylesSecond = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    borderRadius: 30,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-evenly",
    marginHorizontal: "5%",
    width: "90%"
  },
  touchableContainer: {
    width: "100%",
    paddingHorizontal: "3%",
    backgroundColor: Colors.white,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 100,
    paddingVertical: "3%"
  },
  clickable: {
    backgroundColor: Colors.blue,
    elevation: 4
  },
  label: {
    fontSize: 16,
    marginHorizontal: 10,
    color: Colors.darkGray
  },
  icon: {
    tintColor: Colors.gray,
    backgroundColor: Colors.white,
    aspectRatio: 1,
    borderRadius: 10
  }
});
const stylesFirst = StyleSheet.create({
  container: { marginBottom: 8 },
  touchableContainer: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 100,
    height: 40,
    paddingHorizontal: 10
  },
  clickable: {
    backgroundColor: Colors.blue,
    elevation: 4
  },
  noClickable: {
    backgroundColor: Colors.darkGray
  },
  label: {
    fontSize: 20,
    fontWeight: "bold"
  },
  labelOn: {
    color: Colors.white
  },
  labelOff: {
    color: Colors.white
  }
});
