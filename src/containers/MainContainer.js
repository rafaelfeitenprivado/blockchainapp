import React, { Component } from "react";
import { View, StatusBar } from "react-native";
import RootNavigator from "../navigation/RootNavigator";
import { Colors } from "../assets/theme";
import SplashScreen from "react-native-splash-screen";

export default class MainContainer extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    SplashScreen.hide();
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar backgroundColor={Colors.redSecondary} />
        <RootNavigator />
      </View>
    );
  }
}
