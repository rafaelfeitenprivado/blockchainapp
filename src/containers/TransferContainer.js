import React, { Component } from "react";
import ScreenLayout from "../components/ScreenLayout";
import TransferController from "../components/transfer/TransferController";

export default class TransferContainer extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ScreenLayout hidePaddingTop={true} showImage={true}>
        <TransferController navigation={this.props.navigation} />
      </ScreenLayout>
    );
  }
}
