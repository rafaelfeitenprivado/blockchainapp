import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Clipboard,
  Alert,
  TouchableOpacity,
  Image
} from "react-native";
import ScreenLayout from "../components/ScreenLayout";
import { createKeyPars } from "../helpers/databaseHelpers";
import ButtonComponent from "../components/ButtonComponent";
import { Colors } from "../assets/theme";
import { ASSETS } from "../assets";
import {
  screenHeightPercentage,
  screenWidthPercentage
} from "../helpers/utils";
export default class RegisterContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      keyPar: false,
      showError: false
    };
  }

  getKeys = () => {
    this.setState({ isLoading: true });
    createKeyPars()
      .then(resp => {
        this.setState({ isLoading: false, keyPar: resp.dataKey }, () =>
          this.writeToClipboard()
        );
      })
      .catch(erro => {
        this.setState({ isLoading: false, showError: true });
      });
  };

  writeToClipboard = () => {
    Clipboard.setString(this.state.keyPar);
    Alert.alert(
      "Copiado para área de transferência.",
      "Salve suas credenciais em um local seguro. Não nos responsabilizamos com perdas!"
    );
  };

  renderKeys = () => {
    const { keyPar } = this.state;

    if (keyPar) {
      return (
        <View
          style={{
            marginHorizontal: "5%"
          }}
        >
          <View style={{ marginBottom: "5%" }}>
            <Text style={styles.title}>{`Chave privada`}</Text>
            <Text style={styles.value}> {keyPar.privateKey}</Text>
          </View>
          <View style={{ marginBottom: "5%" }}>
            <Text style={styles.title}>{`Chave pública`}</Text>
            <Text style={styles.value}> {keyPar.publicKey}</Text>
          </View>
          <View style={{ marginBottom: "5%" }}>
            <Text style={styles.title}>{`Address`}</Text>
            <Text style={styles.value}> {keyPar.address}</Text>
          </View>
          <View style={{ marginBottom: "5%" }}>
            <Text style={styles.title}>{`WIF`}</Text>
            <Text style={styles.value}> {keyPar.wif}</Text>
          </View>
        </View>
      );
    }
  };

  render() {
    return (
      <ScreenLayout showImage={true}>
        <View style={{ flex: 1 }}>
          <TouchableOpacity
            style={styles.buttonClose}
            onPress={() => this.props.navigation.pop()}
          >
            <Image
              source={ASSETS.icons.arrow}
              style={{
                tintColor: Colors.blue
              }}
            />
          </TouchableOpacity>
          <View
            style={{
              alignItems: "center",
              justifyContent: "center",
              flex: 1
            }}
          >
            {this.renderKeys()}
          </View>
          <View
            style={{
              alignItems: "center",
              justifyContent: "flex-end",
              marginBottom: screenHeightPercentage(15)
            }}
          >
            <ButtonComponent
              label={"Gerar novo par de chaves"}
              onPress={() => this.getKeys()}
              clickable={this.state.isLoading === false}
            />
            <Text style={{ textAlign: "center", marginHorizontal: "5%" }}>
              {`Favor não gerar chaves se não for utilizar, \nuse com consciência!`}
            </Text>
          </View>
        </View>
      </ScreenLayout>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    color: Colors.blue,
    textDecorationLine: "underline",
    fontSize: 16
  },
  value: {
    color: Colors.black
  },
  buttonClose: {
    alignSelf: "flex-start",
    marginTop: screenHeightPercentage(5),
    marginLeft: screenWidthPercentage(1)
  }
});
