import React, { Component } from "react";
import ScreenLayout from "../components/ScreenLayout";
import HomeController from "../components/home/HomeController";
import moment from "moment";

export default class HomeContainer extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    let moment = require("moment");
    require("moment/locale/pt-br");
    moment.locale("pt-br");
  }

  render() {
    return (
      <ScreenLayout hidePaddingTop={true} showImage={true}>
        <HomeController navigation={this.props.navigation} />
      </ScreenLayout>
    );
  }
}
