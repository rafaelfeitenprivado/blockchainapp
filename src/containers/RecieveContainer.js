import React, { Component } from "react";
import ScreenLayout from "../components/ScreenLayout";
import RecieveController from "../components/recieve/RecieveController";

export default class RecieveContainer extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ScreenLayout hidePaddingTop={true} showImage={true}>
        <RecieveController navigation={this.props.navigation} />
      </ScreenLayout>
    );
  }
}
