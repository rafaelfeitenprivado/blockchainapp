import React, { Component } from "react";
import { View, Text } from "react-native";
import InitialController from "../components/initial/InitialController";
import ScreenLayout from "../components/ScreenLayout";

export default class InitialContainer extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ScreenLayout showImage={true}>
        <InitialController navigation={this.props.navigation} />
      </ScreenLayout>
    );
  }
}
