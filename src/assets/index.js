const ASSETS = {
  icons: {
    arrow: require("./icons/arrow/arrow.png"),
    arrowExit: require("./icons/arrowExit/arrowExit.png"),
    camera: require("./icons/camera/camera.png"),
    arrowExit: require("./icons/arrowExit/arrowExit.png"),
    gear: require("./icons/gear/gear.png"),
    bag: require("./icons/bag/bag.png"),
    home: require("./icons/home/homeNew.png"),
    invoice: require("./icons/invoice/invoice.png"),
    map: require("./icons/map/map.png"),
    clock: require("./icons/clock/clock.png"),
    order: require("./icons/order/order.png"),
    receipt: require("./icons/receipt/receipt.png"),
    message: require("./icons/message/message.png"),
    user: require("./icons/user/user.png"),
    eyeNotShow: require("./icons/eye/eyeNotShow.png"),
    eyeShow: require("./icons/eye/eyeShow.png"),
    qrcode: require("./icons/qrcode/qrcodeNew.png"),
    qrcodeBG: require("./icons/qrcode/qrcodeNew (fundo).jpeg"),
    wallet: require("./icons/wallet/wallet.png"),
    walletNew: require("./icons/wallet/wallet.png"),
    transfer: require("./icons/transfer/creditcard.png"),
    flash: require("./icons/flash/flash.png"),
    moneyIn: require("./icons/money-in/money-in.png"),
    moneyOut: require("./icons/money-out/money-out.png"),
    sad: require("./icons/sad/sad.png"),
    checked: require("./icons/checked/checked.png"),
    aceita: require("./icons/aceppted/aceppted.png"),
    paga: require("./icons/payed/payed_new.png"),
    rejeitada: require("./icons/rejected/rejected.png"),
    cancelada: require("./icons/canceled/canceled.png"),
    entregue: require("./icons/sended/sended.png"),
    estornada: require("./icons/reversed/reversed_new.png"),
    bemvindo: require("./icons/bemvindo/bemvindo.png"),
    exclude: require("./icons/exclude/exclude.png"),
    world: require("./icons/world/world.png"),
    action: require("./icons/action/action.png"),
    add: require("./icons/add/add.png"),
    world: require("./icons/world/world.png"),
    historico: require("./icons/historico/historico.png"),
    transation: require("./icons/transastion/transation.png"),
    smartphone: require("./icons/smartphone/smartphone.png"),
    group: require("./icons/group/group.png"),
    email: require("./icons/email/email.png"),
    cnpj: require("./icons/cnpj/cnpj.png"),
    settings: require("./icons/settings/settings.png"),
    exit: require("./icons/exit/exit.png"),
    padlock: require("./icons/padlock/padlock.png"),
    key: require("./icons/key/key.png")
  },
  images: {
    logo: require("./images/logo/logo.png"),
    creditcard: require("./images/creditcard/CreditCard.png"),
    // headerBackground: require("./images/headerBackground/background.jpg"),
    headerBackground: require("./images/headerBackground/background-red.png"),
    background: require("./images/background/background.jpg"),
    bitcoin: require("./images/background/teste.jpg"),
    emptyCartoes: require("./images/emptyBackgrounds/cartoes.png"),
    emptyTransacoes: require("./images/emptyBackgrounds/transacoes.png")
  }
};

module.exports = {
  ASSETS
};
