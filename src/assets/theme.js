const Colors = {
  blue: "#3DA0EC",
  red: "#b12025",
  green: "#60C73B",
  bgBlue: "#b5edff",
  bgRed: "#ffc7c8",
  bgGreen: "#9bf697",
  bgPurple: "rgb(220, 220, 220)",
  greenDark: "green",
  greenOpacity: "#abe8a9",
  greenLight: "#3fc63a",
  greenAction: "#0ad98c",
  redLight: "#c13939",
  redOpacity: "#f2a2a2",
  purple: "#723082",
  yellow: "#F99A2D",
  redPrimary: "#b12025",
  redSecondary: "#891115",
  redAction: "#cf2d33",
  black: "rgb(0,0,0)",
  lightBlack: "rgb(70,70,70)",
  white: "#fff",
  offWhite: "rgb(252, 255, 252)",
  grayPrimary: "#f1f2f2",
  gray: "rgb(220, 220, 220)",
  grayLabel: "#959595",
  grayBg: "#e5e5e5",
  grayLight: "#f5f5f5",
  grayLabelDark: "#565656",
  lightGray: "rgb(240, 240, 240)",
  darkGray: "rgb(174, 174, 174)",
  darkGraySecondary: "#6d6d6d",
  red: "rgb(226, 17, 43)",
  transparent: "transparent"
};

module.exports = {
  Colors
};
