import { AppRegistry } from "react-native";
import { name } from "./app.json";
import MainContainer from "./src/containers/MainContainer";
import HomeContainer from "./src/containers/HomeContainer";
AppRegistry.registerComponent(name, () => MainContainer);
